-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 
-- サーバのバージョン： 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spacejump`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `ranking`
--

CREATE TABLE `ranking` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Score` int(20) NOT NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `ranking`
--

INSERT INTO `ranking` (`Id`, `Name`, `Score`, `Date`) VALUES
(1, 'AAA', 120, '2021-02-09 17:53:44'),
(2, 'BBB', 600, '2021-02-09 08:54:50'),
(3, 'CCC', 720, '2021-02-09 11:58:02'),
(4, 'テストプレイ', 6960, '2021-02-09 12:19:15'),
(5, 'テスト2', 240, '2021-02-09 14:36:41'),
(6, 'test', 4680, '2021-02-18 13:09:33'),
(7, 'test2', 1440, '2021-02-18 13:20:46'),
(8, 'test3', 1680, '2021-02-18 13:21:58'),
(9, 'abc', 1680, '2021-02-18 13:23:53'),
(10, 'abc', 1920, '2021-02-18 14:15:11'),
(11, 'fff', 120, '2021-02-18 14:17:12'),
(12, 'ggg', 120, '2021-02-18 14:26:43'),
(13, '1', 0, '2021-02-18 14:28:41'),
(14, '2', 360, '2021-02-18 14:32:30'),
(15, 'TEST5', 240, '2021-02-18 14:57:41'),
(16, 'int', 120, '2021-02-18 14:58:47'),
(17, '完成', 1200, '2021-02-18 15:07:38'),
(18, 'TEST', 480, '2021-02-18 15:11:25'),
(19, 'TEST', 1680, '2021-02-18 15:17:51'),
(20, 'ひらがな', 4800, '2021-02-18 15:19:58'),
(21, 'a', 600, '2021-02-18 15:21:58'),
(22, 'B', 720, '2021-02-18 15:23:28'),
(23, 'XXX', 960, '2021-02-18 15:28:04'),
(24, 'true', 2040, '2021-02-18 16:42:30'),
(25, 'MIKE', 2520, '2021-02-19 00:34:52'),
(26, 'TRY', 4200, '2021-02-19 00:37:25'),
(27, 'ask', 360, '2021-02-19 00:40:52'),
(28, 'play', 6600, '2021-02-19 01:39:46'),
(29, 'test', 600, '2021-02-19 02:02:58'),
(30, 'TEST2', 2760, '2021-02-19 02:03:50'),
(31, 'Unity', 1920, '2021-02-19 02:06:44'),
(32, 'Unity', 5760, '2021-02-19 02:09:36'),
(33, 'Unity2', 360, '2021-02-19 02:10:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ranking`
--
ALTER TABLE `ranking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

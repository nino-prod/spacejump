﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    [SerializeField] private GameObject Floor = default;
    [SerializeField] private GameObject[] step = new GameObject[8];
    [SerializeField] private GameObject mainCamera = default;
    [SerializeField] private GameObject player = default;
    [SerializeField] private GameObject deathEffect = default;

    public Text scoreText;
    public Text gameOverText;
    public Text countdownText;

    public GameObject inputName;
    public GameObject resistButton;

    private int score = 0;
    private float scoreflgPos = 2.5f;
    private float playerY;
    private Transform cameraPos;
    private Transform playerPos;

    private bool gameOverFlg = false;
    

    void Start()
    {
        //UIの一部を非表示にする
        gameOverText.GetComponent<Text>().enabled = false;
        countdownText.GetComponent<Text>().enabled = false;
        inputName.SetActive(false);
        resistButton.SetActive(false);
        
        cameraPos = mainCamera.GetComponent<Transform>();
        playerPos = player.GetComponent<Transform>();

        StartCoroutine(CountDown());
        
        //ゲーム開始時ランダムに足場を生成する
        for (int i = 1; i < step.Length; i++)
        {
            step[i] = Instantiate(Floor, new Vector2(Random.Range(-3.0f, 3.0f), 2.5f * i), Floor.transform.rotation);
        }       
    }

    
    void Update()
    {
        playerY = playerPos.position.y;
        
        CreateStep();
        ScoreUp();
        GameOverFallen();


        //ゲームオーバーになった時、コルーチンを開始
        if(gameOverFlg == true)
        {
            StartCoroutine(RegistScore());
        }
    }


    void CreateStep()
    {
        
        for (int i = 0; i < step.Length; i++)
        {
            //プレイヤーのY軸の位置を数字分下回ったら、画面上部に足場を移動させる
            if (step[i].gameObject.transform.position.y < playerY - 6)
            {
                step[i].gameObject.transform.position = new Vector2(Random.Range(-3.0f, 3.0f), step[i].gameObject.transform.position.y + 17.5f);
            }
        }

    }

    void ScoreUp()
    {
        //プレイヤーのY軸がscoreflgPosを上回った時、次の加点位置を上げ、
        //スコアを120点プラスし、UIに反映させる
        if (playerY >= scoreflgPos)
        {
            scoreflgPos += 2.8f;
            score += 120;
            scoreText.text = score.ToString("D8");
        }

    }

    
    // 落下時のゲームオーバーのメソッド
    public void GameOverFallen()
    {
        float cameraY = cameraPos.position.y;

        if(playerY <  cameraY - 8)
        {
            player.SetActive(false);
            
            gameOverFlg = true;
        }

    }

    // 弾に当たった時のゲームオーバーのメソッド
    public void GameOverHitBullet()
    {
        player.SetActive(false);
        GameObject effect = Instantiate(deathEffect, playerPos.position, Quaternion.identity);
        Destroy(effect, 0.6f);
        
        gameOverFlg = true;
    }

    private IEnumerator CountDown()
    {
        // ゲーム開始時にカウントダウンをする
        yield return new WaitForSeconds(1);

        countdownText.enabled = true;
        countdownText.text = "3";
        yield return new WaitForSeconds(1);

        countdownText.text = "2";
        yield return new WaitForSeconds(1);

        countdownText.text = "1";
        yield return new WaitForSeconds(1);

        countdownText.text = "GO!";
        GameObject.Find("Player").GetComponent<PlayerController>().isActive = true;
        yield return new WaitForSeconds(1);

        countdownText.enabled = false;
    }
    


    private IEnumerator RegistScore()
    {
        // スコア登録用のUIを表示する
        yield return new WaitForSeconds(1);

        gameOverText.GetComponent<Text>().enabled = true;

        yield return new WaitForSeconds(2);
        inputName.SetActive(true);
        resistButton.SetActive(true);
        

    }

    public void OnClickRetryButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnClickHomeButton()
    {
        SceneManager.LoadScene("TitleScene");
    }
}

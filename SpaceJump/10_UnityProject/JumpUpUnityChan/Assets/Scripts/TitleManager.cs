﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{
    public GameObject board;

    private void Start()
    {
        board.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            board.SetActive(false);
        }
    }

    public void OnClickGameStartButton()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void OnCllickExplainButton()
    {
        board.SetActive(true);
    }
}

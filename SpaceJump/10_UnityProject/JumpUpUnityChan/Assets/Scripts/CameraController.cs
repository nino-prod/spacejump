﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player = default;
    public Transform playerTrans = default;

    
    void Start()
    {
        playerTrans = player.GetComponent<Transform>();
    }

    
    void Update()
    {
        float playerY = playerTrans.position.y + 2;
        float cameraY = transform.position.y;
        float afterjumpY = Mathf.Lerp(cameraY, playerY, 0.5f);

        if(playerY > cameraY)
        {
            transform.position = new Vector3(transform.position.x, afterjumpY, transform.position.z);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingData
{
    public string Name { get; set; }
    public int Score { get; set; }

    public RankingData()
    {
        Name = "";
        Score = 0;
    }
}

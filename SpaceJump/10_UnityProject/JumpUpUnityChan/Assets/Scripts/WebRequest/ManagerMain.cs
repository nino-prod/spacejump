﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;
using System;
using System.IO;
using UnityEngine.Networking;

public class ManagerMain : MonoBehaviour
{
    public Text inputName;
    public Text currentScore;
    public Text setRankingBtn;

    public GameObject rankingBoard;
    public GameObject rankingNode;

    private List<RankingData> rankingList;

    
    public void Start()
    {
        rankingBoard.SetActive(false);
    }
    

    public void GetJsonFromWebRequest()
    {
        StartCoroutine(
                DownloadJson(
                    CallbackWebRequestSuccess,
                    CallbackWebRequestFailed));
    }

    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        // 指定URLからJSONファイルをダウンロードする
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/space_jump/ranking/getranking/");

        yield return www.SendWebRequest();

        if (www.error != null)
        {
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
                
            }
        }
    }

    private void CallbackWebRequestSuccess(string res)
    {
        // JSONを取得出来たらOutputRankingBoardコルーチンを開始する
        rankingList = RankingDataModel.DeserializeFromJson(res);

        StartCoroutine(OutputRankingBoard());
    }

    private void CallbackWebRequestFailed()
    {
        
    }


    private IEnumerator OutputRankingBoard()
    {
        // ランキングボードを表示する
        yield return new WaitForSeconds(1);

        rankingBoard.SetActive(true);

        int posCount = 0;
        int index = 0;

        // ランキングの内容を生成する
        foreach(var rankingData in rankingList)
        {
            index++;

            // 親オブジェクトのランキングボードにフィールドを生成する
            GameObject rankingObj = Instantiate(rankingNode, rankingBoard.transform.position, Quaternion.identity, rankingBoard.transform);

            // 生成したフィールドに内容を流し込む
            rankingObj.transform.Find("RankingRank").GetComponent<Text>().text += index;
            rankingObj.transform.Find("RankingScore").GetComponent<Text>().text = rankingData.Score.ToString();
            rankingObj.transform.Find("RankingName").GetComponent<Text>().text = rankingData.Name;

            // フィールドの位置を修正している
            Vector3 offset = new Vector3(0, -105f * posCount, 0);
            rankingObj.transform.GetComponent<RectTransform>().position += offset;

            posCount++;
        }
    }



    //スコアとネームをサーバーにPOSTするメソッド
    public void OnclickSetRanking()
    {
        setRankingBtn.text = "処理中...";
        SetJsonFromWww();
    }

    private void SetJsonFromWww()
    {
        //POST先のアドレスを指定
        string sTgtURL = "http://localhost/space_jump/ranking/setranking/";

        string name = inputName.text;
        int score = int.Parse(currentScore.text);

        StartCoroutine(
            SetRanking(sTgtURL,
                       name,
                       score,
                       WebRequestSuccess,
                       WebRequestFailed));
    }


    private IEnumerator SetRanking(string url, string name, int score, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        // 名前とスコアを登録するため、フィールドを生成
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("score", score);

        // POST通信をおこなう
        UnityWebRequest webRequest  = UnityWebRequest.Post(url, form);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            Debug.Log($"Success:{webRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    public void WebRequestSuccess(string res)
    {
        setRankingBtn.text = "完了！";

        //POSTした後にサーバーからランキングデータを取得する
        GetJsonFromWebRequest();
    }

    public void WebRequestFailed()
    {
        setRankingBtn.text = "登録失敗";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;

public class RankingDataModel 
{
    public static List<RankingData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<RankingData>();
        IList jsonList = (IList)Json.Deserialize(sStrJson);

        foreach(IDictionary jsonOne in jsonList)
        {
            var tmp = new RankingData();
            if (jsonOne.Contains("Name"))
            {
                tmp.Name = (string)jsonOne["Name"];
            }
            if (jsonOne.Contains("Score"))
            {
                tmp.Score = (int)(long)jsonOne["Score"];
            }

            ret.Add(tmp);
        }

        return ret;
    }
}

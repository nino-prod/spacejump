﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletController : MonoBehaviour
{
    [SerializeField] private GameObject bulletA = default;
    [SerializeField] private GameObject bulletB = default;
    [SerializeField] private GameObject bulletC = default;
    [SerializeField] private GameObject mainCamera = default;

    public float bulletSpeed = default;
    public int nWay = default;

    private Transform cameraPos = default;
    private float cameraY;
    private int timeCount = 0;
    
    void Start()
    {
        cameraPos = mainCamera.GetComponent<Transform>();
    }

    
    void Update()
    {
        timeCount += 1;

        if (timeCount % 120 == 0)
        {
            AttackA();
        }
        if (timeCount % 360 == 0)
        {
            AttackB();
        }      
        if (timeCount % 600 == 0)
        {
            AttackC();
        }
    }

    

    void AttackA()
    {
        //下方向へ発射
        Vector2 shotR = new Vector2(0.0f, -1.0f);
        //ランダムな角度に設定する
        shotR = Quaternion.Euler(0, 0, Random.Range(-15f, 15f)) * shotR;
        //生成位置もランダムにすることで弾の移動範囲を広げる
        GameObject bltA = Instantiate(bulletA, new Vector2(Random.Range(-3f, 3f), cameraPos.position.y + 10), Quaternion.identity);
        
        bltA.GetComponent<Rigidbody2D>().velocity = shotR * bulletSpeed;

        Destroy(bltA, 10f);
    }

    void AttackB()
    {
        Vector2 shotB = new Vector2(0.0f, -1.0f);
        shotB = Quaternion.Euler(0, 0, Random.Range(-20.0f, 20f)) * shotB;

        GameObject bltB = Instantiate(bulletB, new Vector2(Random.Range(-2f, 2f), cameraPos.position.y + 10), Quaternion.identity);

        bltB.GetComponent<Rigidbody2D>().velocity = shotB * 4.0f;

        Destroy(bltB, 6f);
    }

    void AttackC()
    {
        Vector2 shotC = new Vector2(0.0f, -1.0f);
        

        for(int i = 0; i < nWay; i++)
        {
            shotC = Quaternion.Euler(0, 7.5f - (7.5f * nWay) + (15 * i), Random.Range(-20.0f, 20f)) * shotC;
            GameObject bltC = Instantiate(bulletC, new Vector2(Random.Range(-1.5f, 1.5f), cameraPos.position.y + 10), Quaternion.identity);
            bltC.GetComponent<Rigidbody2D>().velocity = shotC * 4.0f;
            Destroy(bltC, 6f);
        }
        
    }
}

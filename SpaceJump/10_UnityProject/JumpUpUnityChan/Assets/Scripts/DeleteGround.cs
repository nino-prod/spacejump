﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteGround : MonoBehaviour
{
    [SerializeField] private GameObject player = default;

    void Start()
    {
        
    }

    
    void Update()
    {
        Transform playerPos = player.GetComponent<Transform>();

        if (transform.position.y < playerPos.position.y - 5)
        {
            Destroy(this.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//プレイヤー操作に関するスクリプト

public class PlayerController : MonoBehaviour
{
    public bool isActive = false;

    [SerializeField] float speed = 0;
    [SerializeField] float jumpPower = 0;
    [SerializeField] private GameObject[] groundchkobj = new GameObject[3];

    float dashspeed = 1;
    float moveH = 0;
    bool jump = false;
    bool isGrounded = false;

    Animator anim;
    SpriteRenderer spr;
    Rigidbody2D rb;

    public GameController gameController;


    void Start()
    {
        
        anim = GetComponent<Animator>();
        spr  = GetComponent<SpriteRenderer>();
        rb   = GetComponent<Rigidbody2D>();

        

        //isActive = true;
    }

   
    void Update()
    {
        
        //キー入力の取得

       
        moveH = Input.GetAxisRaw("Horizontal");

        jump = Input.GetKeyDown("space");

        //playerが画面外にはみ出ないようにする
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -3.8f, 3.8f),
                                         transform.position.y,
                                         0);        
    }

    private void FixedUpdate()
    {
        if(isActive == true)
        {
            PlayerAction();
        }
        
    }

    private void PlayerAction()
    {
        GroundCheck();
        //左右移動に関しての設定
        if (moveH != 0)
        {
            float moveAct = Mathf.Sign(moveH);
            dashspeed = speed * moveAct;

            spr.flipX = moveAct < 0 ? true : false;
        }
        else
        {
            dashspeed = 0;
        }

        //ジャンプ時の設定

        if (jump && isGrounded)
        {
            rb.AddForce(Vector2.up * jumpPower);

            isGrounded = false;
        }

        Animation();

        rb.velocity = new Vector2(dashspeed, rb.velocity.y);
    }

    

    private void GroundCheck()
    {
        Collider2D[] groundchk = new Collider2D[groundchkobj.Length];

        for(int i = 0; i<groundchkobj.Length; i++)
        {
            groundchk[i] = Physics2D.OverlapPoint(groundchkobj[i].transform.position);

            if(groundchk[i] != null)
            {
                isGrounded = true;

                return;
            }
        }

        isGrounded = false;
    }

    private void Animation()
    {
        anim.SetFloat("Speed", Mathf.Abs(dashspeed));
        anim.SetBool("Jump", !isGrounded);

        // Debug.Log("isGrounded =" + isGrounded);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Debug.Log(gameObject.name + " : " + "Trigger");

        gameController.GameOverHitBullet();
    }
}
